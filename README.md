# NorthShirahebi
## A Medusa module for [NadekoBot](https://gitlab.com/kwoth/nadekobot)!
Make your own https://nadekobot.readthedocs.io/en/latest/medusa/creating-a-medusa/

### This Medusa pulls images from [waifu.pics](https://waifu.pics/)

![img.png](img.png)